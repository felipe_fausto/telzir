<?php

use Illuminate\Database\Seeder;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = [
            [
                'from' => 1, // 011
                'to' => 2, // 016
                'price' => '1.90'
            ],
            [
                'from' => 2, // 016
                'to' => 1, // 011
                'price' => '2.90'
            ],
            [
                'from' => 1, // 011
                'to' => 3, // 017
                'price' => '1.70'
            ],
            [
                'from' => 3, // 017
                'to' => 1, // 011
                'price' => '2.70'
            ],
            [
                'from' => 1, // 011
                'to' => 4, // 018
                'price' => '0.90'
            ],
            [
                'from' => 4, // 018
                'to' => 1, // 011
                'price' => '1.90'
            ],
        ];

        foreach ( $areas as $area ) {
            \App\Area::create($area);
        }
    }
}
