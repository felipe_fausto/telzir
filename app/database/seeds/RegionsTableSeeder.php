<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            [
                'id' => 1,
                'area_code' => '011'
            ],
            [
                'id' => 2,
                'area_code' => '016'
            ],
            [
                'id' => 3,
                'area_code' => '017'
            ],
            [
                'id' => 4,
                'area_code' => '018'
            ],
        ];

        foreach ( $regions as $region ) {
            \App\Region::create($region);
        }
    }
}
