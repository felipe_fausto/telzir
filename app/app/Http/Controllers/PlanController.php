<?php

namespace App\Http\Controllers;

use App\Plan;
use App\Http\Requests;
use App\Http\Resources\Plan as PlanResource;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = Plan::all();

        return PlanResource::collection($plans);
    }
}
