<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calculate extends Model
{
    public function getCoverageArea( Region $from, Region $to )
    {
        $area = Area::where('from', $from->id)->where('to', $to->id)->get();

        if ( $area->isNotEmpty() ) {
            return $area->first();
        }

        return false;
    }

    public function calculateValues( Area $area, $minutes = 0, Plan $plan = null )
    {
        if ( $plan ) {
            $minutesRemaining = $plan->minutes >= $minutes ? 0 : $minutes - $plan->minutes;
            return ($minutesRemaining * $area->price) * 1.1;
        }

        return $area->price * $minutes;
    }
}
