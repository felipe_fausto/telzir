<?php

namespace App\Http\Controllers;

use App\Calculate;
use App\Plan;
use App\Region;
use App\Http\Resources\Plan as PlanResource;
use Illuminate\Http\Request;

class CalculateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $post = $request->post();

            $from = Region::findOrFail($post['from']);
            $to   = Region::findOrFail($post['to']);

            $plan = Plan::findOrFail($post['plan']);
            $minutes = $post['minutes'];

            $calc = new Calculate();

            $area = $calc->getCoverageArea($from, $to);

            if ( $area ) {
                $with_plan = $calc->calculateValues($area, $minutes, $plan);
                $without_plan = $calc->calculateValues($area, $minutes);

                $values = [
                    'with_plan' => number_format($with_plan, 2,',', '.'),
                    'without_plan' => number_format($without_plan, 2,',', '.'),
                    'plan' => new PlanResource( $plan )
                ];


                return [ 'data' => $values ];
            }

            return response(['message' => 'Fora da área de cobertura']);
        } catch ( \Exception $e ) {
            return response(['message' => 'Valores inválidos']);
        }
    }
}
