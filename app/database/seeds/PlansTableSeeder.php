<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            [
                'name'    => 'FaleMais 30',
                'minutes' => 30
            ],
            [
                'name'    => 'FaleMais 60',
                'minutes' => 60
            ],
            [
                'name'    => 'FaleMais 120',
                'minutes' => 120
            ]
        ];

        foreach ( $plans as $plan ) {
            \App\Plan::create($plan);
        }
    }
}
