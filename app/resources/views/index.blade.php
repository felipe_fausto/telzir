<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script> window.Laravel = { csrfToken: '{{ csrf_token()  }}' }; </script>

    <title> Telzir | Planos para agilizar a sua vida! </title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <base href="{{URL::to('/api')}}" id="api_url">
</head>
<body class="d-flex flex-column h-100">

<div id="app">
    <navbar></navbar>

    <!-- Content -->
    <main role="main">
        <div class="container" style="margin-top: 75px;">
            <div class="row">
                <div class="col-12">
                    <h1 class="mt-4 text-center"> Você decide seu limite! </h1>
                    <p class="text-center">
                        Com a TELZIR, não existe mais longa distância. Maximize os minutos e fale mais com quem você gosta.
                        <br/>
                        Faça a simulação abaixo e se surpreenda com o custo da ligação ;)
                    </p>
                </div> <!-- .col-12 -->
            </div> <!-- .row -->

            <calculator></calculator>
        </div> <!-- .container -->
    </main> <!-- #main -->
</div> <!-- #app -->

</body>

<script src="{{url('js/app.js')}}"></script>
</html>
<script>
    import Calculator from "../js/components/Plans";
    export default {
        components: {Calculator}
    }
</script>
<script>
    import Navbar from "../js/components/Navbar";
    export default {
        components: {Navbar}
    }
</script>
