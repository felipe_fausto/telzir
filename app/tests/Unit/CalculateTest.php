<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Area;
use App\Plan;
use App\Calculate;
use App\Region;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CalculateTest extends TestCase
{
    public function testInvalidArea()
    {
        $from = new Region;
        $from->id = 1;

        $to = new Region;
        $to->id = 999;

        $calc = new Calculate;

        $area = $calc->getCoverageArea($from, $to);
        $this->assertEquals($area, false);
    }

    public function testPrice()
    {
        $from = new Region;
        $from->id = 1; // 011

        $to = new Region;
        $to->id = 2; //016

        $plan = new Plan;
        $plan->minutes = 30;

        $calc = new Calculate;
        $area = $calc->getCoverageArea($from, $to);

        $this->assertInstanceOf(Area::class, $area);

        $this->assertEquals($calc->calculateValues($area, 100, $plan), 146.3);
        $this->assertEquals($calc->calculateValues($area, 100), 190);
    }
}
