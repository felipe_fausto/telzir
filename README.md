# Telzir

Telzir is a single page where the client can calculate the advantages of getting a FaleMais plan.

## Installation
### Containers and Laravel

This project run over Docker containers with Laravel Framework. To build and run this project you will need to install `Docker` and `docker-compose` on your machine.

The first step is to build up your Docker containers. Go to the project root folder and run the command below:

```
docker-compose up
```

Then you'll need to download all dependencies through the php container. Run the command:

```
docker exec -it telzir-php-fpm bash -c "cd app && composer install"
```

The second step is to create your `.env` file. So you need to create an `.env` file inside the folder `app` (you can copy the `.env.example` file and rename it).

Inside the `.env` file change the values
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```
to
```
DB_CONNECTION=mysql
DB_HOST=telzir-mysql
DB_PORT=3306
DB_DATABASE=telzir
DB_USERNAME=root
DB_PASSWORD=root
```
Save it and close it.

The third and last step to run Laravel properly is to run the command below to generate the `APP_KEY`
```
docker exec -it telzir-php-fpm bash -c "cd app && php artisan key:generate"
```

### Database

When you run the Docker containers the default database created will be `telzir`. So now all we need to do is to run the migrations and seeds.

To run the migration and create all tables run the command
```
docker exec -it telzir-php-fpm bash -c "cd app && php artisan migrate"
```
And to seed the tables run
```
docker exec -it telzir-php-fpm bash -c "cd app && php artisan db:seed"
```

After all that you will be able to use the system properly.

Have fun ;)
