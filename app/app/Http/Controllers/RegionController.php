<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Region;
use App\Http\Resources\Region as RegionResource;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = Region::all();
        return RegionResource::collection($regions);
    }
}
